//
//  RopeSprite.swift
//  TugOfWarTV
//
//  Created by Murilo da Paixão on 09/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import SpriteKit

class RopeSprite: SKNode {
    
    fileprivate var ropeNodes = [RopeNode]()
    fileprivate var ropeParts = [SKSpriteNode]()
    
    var length: Int?
    
    var rightTeam : [SKSpriteNode]!
    var leftTeam  : [SKSpriteNode]!
    
    var headNode: RopeNode? {
        get {
            return ropeNodes.first
        }
    }
    
    var tailNode: RopeNode? {
        get {
            return ropeNodes.last
        }
    }
    
    init(withLeftTeam leftTeam: [SKSpriteNode], rightTeam: [SKSpriteNode], length: Int) {
        super.init()
        
        self.rightTeam = rightTeam
        self.leftTeam  = leftTeam
        
        self.length = length
    }
    
    func renderRope() {
        loadRopeParts()
        connectRopeParts()
    }
    
    fileprivate func loadRopeParts() {
        
        for i in 0...length! {
            
            let node = RopeNode(SKTexture(image: #imageLiteral(resourceName: "rope_node")))
            
            node.position = CGPoint(x: rightTeam.first!.position.x  - (CGFloat(i)*node.frame.size.width), y: leftTeam.last!.position.y)
            
            ropeNodes.append(node)
            ropeParts.append(node)
            scene!.addChild(node)
        }
        
        rightTeam.first!.position = CGPoint(x: headNode!.position.x, y: tailNode!.frame.midY)
        
        for (index, player) in rightTeam.enumerated() {
            player.position = CGPoint(x: rightTeam.first!.position.x  - (CGFloat(index*5)*headNode!.size.width), y: rightTeam.first!.position.y)
            self.ropeParts.insert(player, at: index*5)
        }
        
        leftTeam.first!.position = CGPoint(x: tailNode!.position.x, y: tailNode!.frame.midY)
        ropeParts.append(leftTeam.first!)
        
        for index in (1..<leftTeam.count) {
            let player = leftTeam[index]
            
            player.position = CGPoint(x: leftTeam.first!.position.x  + (CGFloat(index*5)*headNode!.size.width), y: leftTeam.first!.position.y)
            self.ropeParts.insert(player, at: (ropeParts.count - 1) - index*5)
        }

    }
    
    fileprivate func connectRopeParts() {
        for i in (1..<ropeParts.count) {
            let nodeA = ropeParts[i-1]
            let nodeB = ropeParts[i]
            
            self.scene!.physicsWorld.add(makeJoint(nodeA: nodeA, nodeB: nodeB))
        }
    }
    
    fileprivate func makeJoint(nodeA: SKNode, nodeB: SKNode) -> SKPhysicsJoint {
        let anchor = scene!.convert(CGPoint(x: nodeA.frame.minX, y: nodeA.frame.midY), from: nodeA.parent!)
        
        let joint = SKPhysicsJointFixed.joint(withBodyA: nodeA.physicsBody!, bodyB: nodeB.physicsBody!, anchor: anchor)
        
        return joint
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}











