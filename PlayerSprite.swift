//
//  PlayerSprite.swift
//  TugOfWarTV
//
//  Created by Murilo da Paixão on 10/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import SpriteKit

class PlayerSprite: SKSpriteNode {
    
    init(_ texture: SKTexture) {
        super.init(texture: texture, color: UIColor(), size: texture.size())
        addPhysics()
    }
    
    init(withSize size: CGSize, color: UIColor) {
        super.init(texture: nil, color: color, size: size)
        addPhysics()
    }
    
    fileprivate func addPhysics() {
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        self.physicsBody?.affectedByGravity = true
        self.physicsBody?.allowsRotation = false
        self.physicsBody?.isDynamic = true
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
