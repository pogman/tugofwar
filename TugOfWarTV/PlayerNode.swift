//
//  Torso.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 23/02/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation
import SpriteKit

class PlayerNode: BodyNode {
    
    var team : Colors!
    var head : HeadNode!
    var arm  : ArmNode!
    var jet  : SKEmitterNode!
    
    var pullingForce: Double = 50
    
    init(ofTeam team: Colors) {
        
        let bodyTexture = (team == .red) ? SKTexture(image: #imageLiteral(resourceName: "RedBody")) : SKTexture(image: #imageLiteral(resourceName: "BlueBody"))
        
        super.init(texture: bodyTexture)
        
        self.team = team
        loadTextures(ofTeam: self.team)
        
        self.limbs = [head, arm]
    }
    
    private func loadTextures(ofTeam team: Colors) {
        
        switch team {
            
        case .red:
            self.head = HeadNode(texture: SKTexture(image: #imageLiteral(resourceName: "RedHead")))
            self.arm  = ArmNode(texture: SKTexture(image: #imageLiteral(resourceName: "RedFrontArm")))
            arm.team = self.team
            
            // Load Jet particles colors
            self.jet = SKEmitterNode(fileNamed: "RedJet.sks")
            
            self.jet.position = CGPoint(x: 0, y: -50)
            self.jet.zPosition = -1
            self.addChild(jet)
            
        case .blue:
            self.head = HeadNode(texture: SKTexture(image: #imageLiteral(resourceName: "BlueHead")))
            self.arm  = ArmNode(texture: SKTexture(image: #imageLiteral(resourceName: "BlueFrontArm")))
            arm.team = self.team
            
            // Load Jet particles colors
            self.jet = SKEmitterNode(fileNamed: "BlueJet.sks")
            
            self.jet.position = CGPoint(x: 0, y: -50)
            self.jet.zPosition = -1
            self.addChild(jet)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
