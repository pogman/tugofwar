//
//  ArmNode.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 21/02/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation
import SpriteKit

class ArmNode: SKSpriteNode, Limb {
    
    var team: Colors = .blue
    
    init(texture: SKTexture?) {
        super.init(texture: texture, color: UIColor(), size: texture!.size())
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.85)
        
        self.physicsBody = SKPhysicsBody(rectangleOf: size, center: self.centerPoint)
        self.physicsBody?.collisionBitMask = 0x1 << 1
        self.physicsBody?.categoryBitMask = 0x1 << 1
        self.physicsBody?.mass = 0.00015
        
        self.zPosition = 2
    }
    
    func attach(toBody body: BodyNode) {
        
        let anchorPoint = body.convert(CGPoint(x: body.frame.midX, y: body.frame.maxY / 1.8), to: body.scene!)
        
        self.position = anchorPoint
        
        let joint =  SKPhysicsJointPin.joint(withBodyA: self.physicsBody!, bodyB: body.physicsBody!, anchor: anchorPoint)
        
        joint.shouldEnableLimits = true
        
        switch(self.team) {
        case .blue:
            joint.lowerAngleLimit = -90.radianValue
            joint.upperAngleLimit = -15.radianValue
            
        case .red:
            joint.lowerAngleLimit = 15.radianValue
            joint.upperAngleLimit = 90.radianValue
        }
        body.scene!.physicsWorld.add(joint)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
