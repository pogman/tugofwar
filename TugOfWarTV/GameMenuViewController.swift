//
//  GameMenuViewController.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 21/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import UIKit
import Foundation

protocol GameMenuDelegate: class {
    func tryAgain()
    func goToMenu()
}

class GameMenuViewController: UIViewController {
    
    @IBOutlet weak var playAgain: UIButton!
    @IBOutlet weak var menu: UIButton!
    @IBOutlet weak var labelVictory: UILabel!
    
    var screen = (UIApplication.shared.delegate?.window)!
    var viewMenu: UIView!
    
    weak var delegate: GameMenuDelegate!
    
    override func viewDidLoad() {

    }
    
    func createView() {
        
        screen = (UIApplication.shared.delegate?.window)!
        
        let KeyboardExerciseNib = UINib(nibName: "GameMenu", bundle: nil)
        viewMenu = KeyboardExerciseNib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        self.viewMenu.frame = CGRect(x: 0, y: (self.screen?.frame.height)!, width: (self.screen?.frame.width)!, height: (self.screen?.frame.height)!)
        
        viewMenu.layer.zPosition = 30
        
        
        if let imageView = playAgain.imageView {
            imageView.adjustsImageWhenAncestorFocused = true
            imageView.clipsToBounds = false
        }
        
        if let imageView = menu.imageView {
            imageView.adjustsImageWhenAncestorFocused = true
            imageView.clipsToBounds = false
        }
        
        screen?.addSubview(viewMenu)
        
    }

    
    @IBAction func btnMenuAction(_ sender: Any) {
        self.view.removeFromSuperview()
        delegate.goToMenu()
    }

    @IBAction func btnTryAgain(_ sender: Any) {
        delegate.tryAgain()
    }
    
    func show(textVictory: String) {
        self.createView()
        self.labelVictory.text = textVictory
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.viewMenu.frame = CGRect(x: 0, y: 0, width: (self.screen?.frame.width)!, height: (self.screen?.frame.height)!)
        })
    }
    
    func hide() {
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.viewMenu.frame = CGRect(x: 0, y: (self.screen?.frame.height)!, width: (self.screen?.frame.width)!, height: (self.screen?.frame.height)!)
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.viewMenu.removeFromSuperview()
        }
        self.view.removeFromSuperview()
    }
}

