//
//  GameModeViewController.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 21/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class GameModeViewController: UIViewController {
    
    @IBOutlet weak var btnSingle: UIButton!
    @IBOutlet weak var btnMulti: UIButton!
    let gameMenu = GameMenuViewController()

    //@IBOutlet var unfocusedConstraintSingle: NSLayoutConstraint!
    //@IBOutlet var unfocusedConstraintMulti: NSLayoutConstraint!
    
    //@IBOutlet weak var lbName: UILabel!
    
    
    var focusedConstraintSingle: NSLayoutConstraint!
    var focusedConstraintMulti: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let imageView = btnSingle.imageView {
            imageView.adjustsImageWhenAncestorFocused = true
            imageView.clipsToBounds = false
        }
        
        if let imageView = btnMulti.imageView {
            imageView.adjustsImageWhenAncestorFocused = true
            imageView.clipsToBounds = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let _ = song {
            song?.stop()
        }
        gameScene?.view?.isPaused = true
    }
    
    @IBAction func btnSinglePlayerAction(_ sender: Any) {
        showAlertSelectLevelGame()
    }
    
    func showAlertSelectLevelGame() { // 1
        let alertController = UIAlertController(title: "Single Player", message: "Select a Level Game", preferredStyle: .alert) // 2
        
        let automaticLevel = UIAlertAction(title: "Automatic", style: .default) { (action) in
            self.createSinglePlayerTeams(machineLevel: .automatic)
        }
        
        let easyLevel = UIAlertAction(title: "Easy", style: .default) { (action) in
            self.createSinglePlayerTeams(machineLevel: .easy)
        }
        
        let mediumLevel = UIAlertAction(title: "Medium", style: .default) { (action) in
            self.createSinglePlayerTeams(machineLevel: .medium)
        }
        
        let hardLevel = UIAlertAction(title: "Hard", style: .default) { (action) in
            self.createSinglePlayerTeams(machineLevel: .hard)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in //3
        }
        
        alertController.addAction(automaticLevel)
        alertController.addAction(easyLevel)
        alertController.addAction(mediumLevel)
        alertController.addAction(hardLevel)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) { // 5
        }
    }
    
    func createSinglePlayerTeams(machineLevel: MachinePlayerLevel){
        let player = HumanPlayer(name: "Human", remote: RemotePlayer(id: "remote", name: "Remote", peerID: nil))
        let playerMachine = MachinePlayer(level: machineLevel)
        
        let team1 = Team(name: "Time 1", color: .blue, players: [player])
        let team2 = Team(name: "Machine Team", color: .red, players: [playerMachine])
        
        appDelegate.gamePlay = GamePlay(teams: [team1 , team2])
        self.performSegue(withIdentifier: "ToPlay", sender: nil)
    }
}

