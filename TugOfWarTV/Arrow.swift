//
//  File.swift
//  TugOfWar
//
//  Created by Luisa Macedo on 2/8/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import SpriteKit

enum Directions: Int {
    case bottom = 0, right, top, left, none
}

enum Colors: Int {
    case red = 0, blue
}

class Arrow: SKSpriteNode {
    var directionOfArrow: Directions?
    var screenSize: CGSize?
    var colorArrow: Colors?
    var velocity: CGVector?
    var flagSetScoreMachine = 0
    
    convenience init(typeOfDirection: Directions, screenSize: CGSize, colorArrow: Colors, velocity: CGVector) {
        self.init(texture: nil, color: SKColor.clear, size: CGSize(width: 65, height: 65))
        self.directionOfArrow = typeOfDirection
        self.screenSize = screenSize
        self.colorArrow = colorArrow
        self.velocity = velocity
        setImageArrow()
        createPhysicsBody()
    }
    
    
    fileprivate override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImageArrow() {
        texture = SKTexture(imageNamed: "\(directionOfArrow!)\(colorArrow!)")
    }
    
    func createPhysicsBody() {
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        self.physicsBody?.categoryBitMask = PhysicsCategory.Arrow
        self.physicsBody?.contactTestBitMask = PhysicsCategory.BottomScreen | PhysicsCategory.CurrentDirection | PhysicsCategory.FadeOut | PhysicsCategory.FadeIn | PhysicsCategory.CheckMachine | PhysicsCategory.CheckReceveid
        self.physicsBody?.collisionBitMask = PhysicsCategory.None
        self.physicsBody?.affectedByGravity = false
    }
}
