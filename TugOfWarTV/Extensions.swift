//
//  Extensions.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 10/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import SpriteKit

extension Data {
    
    init<T>(from value: T) {
        var value = value
        self.init(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }
    
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.pointee }
    }
}

extension String {
    func slice(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
    
    var gadgetName: String {
        if (self.slice(from: "i", to: "de")) != nil {
            if "\(self.slice(from: "i", to: "de")!)" == "Phone " {
                var componentes = self.components(separatedBy: " de ")
                return componentes[1]
            }
        } else if (self.slice(from: "’", to: "ne")) != nil {
            if "\(self.slice(from: "’", to: "ne")!)" == "s iPho" {
                var componentes = self.components(separatedBy: "’s ")
                return componentes[0]
            }
        }
        return self
    }
}

extension SKSpriteNode {
    
    var centerPoint: CGPoint {
        return CGPoint(x: self.size.width / 2 - (self.size.width * self.anchorPoint.x), y: self.size.height / 2 - (self.size.height * self.anchorPoint.y))
    }
}

extension Int {
    var radianValue: CGFloat {
        return CGFloat((Double(self) * M_PI) / 180)
    }
}
