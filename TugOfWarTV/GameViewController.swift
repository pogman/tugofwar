//
//  GameViewController.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 08/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation
import GameplayKit

var gameScene: GameScene?
var song: AVAudioPlayer?

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToDifferentView), name: NSNotification.Name(rawValue: "segue") , object: nil)
        
        // Load 'GameScene.sks' as a GKScene. This provides gameplay related content
        // including entities and graphs.
        
        if let scene = GameScene(fileNamed: "GameScene") {
            
            scene.gameVC = self
            scene.scaleMode = .aspectFill

            // Get the SKScene from the loaded GKScene
            //if let sceneNode = scene.rootNode as! GameScene? {
                
                // Copy gameplay related content over to the scene
                //sceneNode.entities = scene.entities
                //sceneNode.graphs = scene.graphs
                
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(scene)
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsPhysics = false
                    view.showsFPS = true
                    view.showsNodeCount = true
                }
            gameScene = scene
           // }
        }
    }
    
    func goToDifferentView() {
        //UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        self.performSegue(withIdentifier: "volta", sender: self)
        //self.dismiss(animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
