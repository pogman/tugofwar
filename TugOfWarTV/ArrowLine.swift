//
//  File.swift
//  TugOfWar
//
//  Created by Luisa Macedo on 2/8/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import SpriteKit

class ArrowLine: SKSpriteNode {
    
    var numberOfArrows: Int?
    var distance: CGFloat?
    var screenSize: CGSize?
    var velocity: CGVector?
    var lastArrow: Arrow?
    var player: Player?
    var teamLine: Team?
    var idUser = ""
    var colorLine: Colors?
    
    convenience init(numberOfArrows: Int, distance: CGFloat, screenSize: CGSize, position: CGPoint, velocity: CGVector, colorLine: Colors) {
        self.init(texture: nil, color: SKColor.clear, size: CGSize(width: 50,height: screenSize.height))
        self.numberOfArrows = numberOfArrows
        self.distance = distance
        self.screenSize = screenSize
        self.velocity = velocity
        self.position = position
        self.lastArrow = Arrow()
        self.colorLine = colorLine
        self.name = "line"
        createArrows()
    }
    
    fileprivate override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createArrows() {
        for i in 1...numberOfArrows! {
            let sort = arc4random_uniform(3)
            let arrow = Arrow(typeOfDirection: Directions(rawValue: Int(sort))!, screenSize: self.screenSize!, colorArrow: colorLine!, velocity: velocity!)
            arrow.position = CGPoint(x: self.position.x, y: (lastArrow?.position.y)! + CGFloat(i) * (arrow.size.height + self.distance!))
            if i == 1 {
                arrow.physicsBody?.categoryBitMask = PhysicsCategory.FirstArrow
                arrow.physicsBody?.contactTestBitMask = PhysicsCategory.BottomScreen
                arrow.physicsBody?.collisionBitMask = PhysicsCategory.None
            }
            arrow.run(SKAction.fadeOut(withDuration: 0.2))
            self.addChild(arrow)
            if i == numberOfArrows! {
                self.lastArrow = arrow
            }
        }
    }
    
    func move() {
        if appDelegate.gamePlay.teams[1].players[0].type == .Machine {
            switch (appDelegate.gamePlay.teams[1].players[0] as! MachinePlayer).machineLevel! {
            case MachinePlayerLevel.easy:
                position.y = position.y - 1.5
            case MachinePlayerLevel.medium:
                position.y = position.y - 1.7
            case MachinePlayerLevel.hard:
                position.y = position.y - 2.0
            default:
                position.y = position.y - 1.7
            }
        }else {
            position.y = position.y - 1.7
        }
    }

}
