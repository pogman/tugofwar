//
//  File.swift
//  TugOfWarTV
//
//  Created by Luisa Macedo on 2/9/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import SpriteKit

protocol PatternLoader {
    func loadMoreArrows(line: ArrowLine)
}
