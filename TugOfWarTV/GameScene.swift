//
//  GameScene.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 08/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import SpriteKit
import GameplayKit
import GameController
import AVFoundation
import MultipeerConnectivity

struct PhysicsCategory {
    static let None:                UInt32 = 0
    static let Arrow:               UInt32 = 0x1 << 1
    static let FirstArrow:          UInt32 = 0x1 << 2
    static let BottomScreen:        UInt32 = 0x1 << 3
    static let CurrentDirection:    UInt32 = 0x1 << 4
    static let FadeOut:             UInt32 = 0x1 << 5
    static let FadeIn:              UInt32 = 0x1 << 6
    static let CheckMachine:        UInt32 = 0x1 << 7
    static let CheckReceveid:       UInt32 = 0x1 << 8
}

class GameScene: SKScene, SKPhysicsContactDelegate, UIGestureRecognizerDelegate {
    
    var gameVC: GameViewController?
    var lines = [ArrowLine]()
    var currentDirections = [Directions]()
    var positionsX = [CGFloat]()
    var flagSetMachine = 0
    var labelScoreTeam0: SKLabelNode!
    var labelScoreTeam1: SKLabelNode!
    var labelVictory: SKLabelNode!
    var numberOfUsers = 0
    var endGame = 0
    let textureTeams = SKSpriteNode()
    let gameMenu = GameMenuViewController()
    var targets = [SKSpriteNode]()
    var flagsReceive = [Int]()
    var rightTeam : TeamNode!
    var leftTeam  : TeamNode!
    var rope: Rope!
    
    override func sceneDidLoad() {
        self.physicsWorld.gravity = CGVector(dx: 0, dy: -0.1)
        self.physicsWorld.contactDelegate = self
        appDelegate.mcManager.delegate = self
        appDelegate.gamePlay.delegate = self
        gameMenu.delegate = self
        
        appDelegate.mcManager.browser.stopBrowsingForPeers()
        
        //Set Number Of Users
        let numberOfTeams = appDelegate.gamePlay.teams.count
        if appDelegate.gamePlay.teams[0].players.count >= appDelegate.gamePlay.teams[1].players.count {
            numberOfUsers = appDelegate.gamePlay.teams[0].players.count * 2
        } else {
            numberOfUsers = appDelegate.gamePlay.teams[1].players.count * 2
        }
        
        setRopeAndTeams()
        
        //setLabelVictory
        self.labelVictory = SKLabelNode()
        self.addChild(labelVictory)
        
        //Set Direction
        for _ in 0..<numberOfUsers {
            self.currentDirections.append(.none)
        }
        
        //Set Positions X
        if numberOfUsers == 2 {
            positionsX = [-self.size.width/8, self.size.width/8]
        } else if numberOfUsers == 4 {
            positionsX = [-3 * (self.size.width/16), -self.size.width/16, self.size.width/16, 3 * (self.size.width/16)]
        } else if numberOfUsers == 6 {
            positionsX = [-3 * (self.size.width/16), -2 * (self.size.width/16), -self.size.width/16, self.size.width/16, 2 * self.size.width/16, 3 * self.size.width/16]
        }
        
        self.playSong()
        self.setBackgroundScene()
        
        //Set bottom Screen
        let bottomScreen = SKSpriteNode()
        bottomScreen.physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(x: -self.size.width/2, y: self.size.height/4 - 60, width: self.size.width, height: 5))
        bottomScreen.physicsBody?.categoryBitMask = PhysicsCategory.BottomScreen
        bottomScreen.physicsBody?.contactTestBitMask = PhysicsCategory.FirstArrow
        bottomScreen.physicsBody?.collisionBitMask = PhysicsCategory.None
        self.addChild(bottomScreen)
        
        let colors: [Colors] = [.red, .blue]
        
        for j in 0..<numberOfTeams {
            for i in 0..<numberOfUsers/2 {
                var posX: CGFloat!
                if numberOfUsers == 4 {
                    posX = positionsX[i+j+(1 * j)]
                } else if numberOfUsers == 2 {
                    posX = CGFloat(positionsX[i+j])
                } else if numberOfUsers == 6 {
                    posX = positionsX[i+j+(2 * j)]
                }
                
                let line = ArrowLine(numberOfArrows: 10, distance: 10, screenSize: (self.scene?.size)!, position: CGPoint(x: posX , y: (self.scene?.size.height)!/2 - 10), velocity: CGVector(dx: 0, dy: -80), colorLine: colors[j])
                line.teamLine = appDelegate.gamePlay.teams[j]
                line.player = appDelegate.gamePlay.teams[j].players[i]
                line.name = "line\(self.lines.count)"
                if appDelegate.gamePlay.teams[j].players[i].type == .Human {
                    line.idUser = (appDelegate.gamePlay.teams[j].players[i] as! HumanPlayer).remote.id
                } else if appDelegate.gamePlay.teams[j].players[i].type == .Machine {
                    line.idUser = "Machine"
                }
                self.flagsReceive.append(0)
                self.lines.append(line)
                self.addChild(line)
                
                let currentDirection = SKSpriteNode()
                currentDirection.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                currentDirection.physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(x: 2 * line.position.x, y: 2 * (self.size.height/4 - 50), width: 6, height: 6))
                currentDirection.physicsBody?.categoryBitMask = PhysicsCategory.CurrentDirection
                currentDirection.physicsBody?.contactTestBitMask = PhysicsCategory.FirstArrow | PhysicsCategory.Arrow
                currentDirection.physicsBody?.collisionBitMask = PhysicsCategory.None
                self.addChild(currentDirection)
                
                let checkMachine = SKSpriteNode()
                checkMachine.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                checkMachine.physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(x: 2 * line.position.x, y: -30 + 2 * (self.size.height/4 - 50), width: 6, height: 6))
                checkMachine.physicsBody?.categoryBitMask = PhysicsCategory.CheckMachine
                checkMachine.physicsBody?.contactTestBitMask = PhysicsCategory.FirstArrow | PhysicsCategory.Arrow
                checkMachine.physicsBody?.collisionBitMask = PhysicsCategory.None
                self.addChild(checkMachine)
                
                let checkReceveid = SKSpriteNode()
                checkReceveid.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                checkReceveid.physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(x: 2 * line.position.x, y: -57 + 2 * (self.size.height/4 - 50), width: 6, height: 6))
                checkReceveid.physicsBody?.categoryBitMask = PhysicsCategory.CheckReceveid
                checkReceveid.physicsBody?.contactTestBitMask = PhysicsCategory.FirstArrow | PhysicsCategory.Arrow
                checkReceveid.physicsBody?.collisionBitMask = PhysicsCategory.None
                self.addChild(checkReceveid)
                
                let fadeOut = SKSpriteNode()
                fadeOut.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                fadeOut.physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(x: 2 * line.position.x, y: -72 + 2 * (self.size.height/4 - 50), width: 6, height: 6))
                fadeOut.physicsBody?.categoryBitMask = PhysicsCategory.FadeOut
                fadeOut.physicsBody?.contactTestBitMask = PhysicsCategory.FirstArrow | PhysicsCategory.Arrow
                fadeOut.physicsBody?.collisionBitMask = PhysicsCategory.None
                self.addChild(fadeOut)
                
                let fadeIn = SKSpriteNode()
                fadeIn.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                fadeIn.physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(x: 2 * line.position.x, y: self.size.height/2, width: 6, height: 6))
                fadeIn.physicsBody?.categoryBitMask = PhysicsCategory.FadeIn
                fadeIn.physicsBody?.contactTestBitMask = PhysicsCategory.FirstArrow | PhysicsCategory.Arrow
                fadeIn.physicsBody?.collisionBitMask = PhysicsCategory.None
                self.addChild(fadeIn)
                
                let targetArea = SKSpriteNode(texture: SKTexture(image: #imageLiteral(resourceName: "targetWhite")))
                targetArea.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                targetArea.position = CGPoint(x: 2 * line.position.x, y: 2 * (self.size.height/4 - 50))
                targetArea.size = CGSize(width: 80, height: 80)
                self.targets.append(targetArea)
                self.addChild(targetArea)
            }
        }
        setLabelsScore()
        setLabelsUsers()
        
    }
    
    func setRopeAndTeams() {
    self.rightTeam = TeamNode(of: self.numberOfUsers/2, color: .blue, inside: self)
    self.leftTeam  = TeamNode(of: self.numberOfUsers/2, color: .red,  inside: self)
    if numberOfUsers == 2 {
        rope = Rope(ofSize: .small)
    } else if numberOfUsers == 4 {
        rope = Rope(ofSize: .large)
    } else if numberOfUsers == 6 {
        rope = Rope(ofSize: .veryLarge)
    }
    self.addChild(rope)
    
    rope.draw()
    
    try! rope.attach(leftTeam: leftTeam, rightTeam: rightTeam)
    }
    
    override var canBecomeFocused: Bool {
        return true
    }
    
    /*func animateSprite(spriteArray: [SKTexture]) {
        let animationAction = SKAction.animate(with: spriteArray, timePerFrame: 0.02)
        var moveAction = SKAction()
        if spriteArray == spriteArrayTeam1 {
            moveAction = SKAction.move(to: CGPoint(x:self.textureTeams.position.x - 12, y:self.textureTeams.position.y), duration: 0.01)
        } else {
            moveAction = SKAction.move(to: CGPoint(x:self.textureTeams.position.x + 12, y:self.textureTeams.position.y), duration: 0.01)
        }
        textureTeams.run(SKAction.sequence([moveAction, animationAction]))
    }*/
    
    func animate(team: TeamNode) {
        team.pull()
        if team.color == leftTeam!.color {
            rightTeam.bePulled()
        } else if team.color == rightTeam!.color {
            leftTeam.bePulled()
        }
    }
    
    func setBackgroundScene() {
        let background = SKSpriteNode(texture: SKTexture(image: #imageLiteral(resourceName: "BG-")))
        background.zPosition = -12
        background.anchorPoint = CGPoint(x: 0, y: 0)
        background.position = CGPoint(x: -self.size.width/2 - 10, y: -self.size.height/6)
        background.size = CGSize(width: self.size.width + 20, height: self.size.height * 3/4)
        self.addChild(background)
      
        let citiesPt2 = SKSpriteNode(texture: SKTexture(image: #imageLiteral(resourceName: "CitiesPt2")))
        citiesPt2.zPosition = -10
        citiesPt2.anchorPoint = CGPoint(x: 0, y: 0)
        citiesPt2.size = CGSize(width: self.size.width, height: self.size.width * 0.163)
        citiesPt2.position = CGPoint(x: -self.size.width/2, y: -self.size.height/6 - 10)
        self.addChild(citiesPt2)
        
        let citiesPt1 = SKSpriteNode(texture: SKTexture(image: #imageLiteral(resourceName: "CitiesPt1")))
        citiesPt1.zPosition = -8
        citiesPt1.anchorPoint = CGPoint(x: 0.5, y: 0)
        citiesPt1.size = CGSize(width: self.size.width * 0.985, height: (self.size.width * 0.985) * 0.1625)
        citiesPt1.position = CGPoint(x: 0, y: -self.size.height/6 - 10)
        self.addChild(citiesPt1)

        let ground = SKSpriteNode(texture: SKTexture(image: #imageLiteral(resourceName: "ground1")))
        ground.zPosition = -6
        ground.anchorPoint = CGPoint(x: 0, y: 0)
        ground.size = CGSize(width: self.size.width, height: self.size.height/3)
        ground.position = CGPoint(x: -self.size.width/2, y: -self.size.height/2)
        self.addChild(ground)
        
    }
    
    override func didMove(to view: SKView) {
        // Register Swipe Events
        
        let swipeRight:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedRight(_:)))
        swipeRight.direction = .right
        view.addGestureRecognizer(swipeRight)
        
        
        let swipeLeft:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedLeft(_:)))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
        
        
        let swipeUp:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedUp(_:)))
        swipeUp.direction = .up
        view.addGestureRecognizer(swipeUp)
        
        
        let swipeDown:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedDown(_:)))
        swipeDown.direction = .down
        view.addGestureRecognizer(swipeDown)
    }
    
    // Handle Swipe Events
    func swipedRight(_ gesture: UISwipeGestureRecognizer){
        print("right")
        self.flagsReceive[0] = 1
        verifyGestureRemote(controller: .right)
    }
    
    func swipedLeft(_ gesture: UISwipeGestureRecognizer){
        print("left")
        self.flagsReceive[0] = 1
        verifyGestureRemote(controller: .left)
    }
    
    func swipedUp(_ gesture: UISwipeGestureRecognizer){
        print("up")
        self.flagsReceive[0] = 1
        verifyGestureRemote(controller: .top)
    }
    
    func swipedDown(_ gesture: UISwipeGestureRecognizer){
        print("down")
        self.flagsReceive[0] = 1
        verifyGestureRemote(controller: .bottom)
    }
    
    private func verifyGestureRemote(controller: Directions){
        if controller == self.currentDirections.first {
            appDelegate.gamePlay.teams.first!.players[0].sumScore()
            setTargetTexture(image: #imageLiteral(resourceName: "targetGreen"), indexLine: 0, width: 100)
            self.animate(team: leftTeam)
        } else {
            setTargetTexture(image: #imageLiteral(resourceName: "targetRed"), indexLine: 0, width: 100)
        }
        print("\(controller) = \(self.currentDirections.first!)")
    }
    
    func setLabelsUsers() {
        for j in 0..<appDelegate.gamePlay.teams.count {
            for i in 0..<numberOfUsers/2 {
                let labelUser = SKLabelNode()
                labelUser.fontColor = .white
                labelUser.fontSize = 15
                labelUser.horizontalAlignmentMode = .center
                labelUser.verticalAlignmentMode = .center
                
                if numberOfUsers == 4 {
                    labelUser.position = CGPoint(x: self.lines[i+j+(1 * j)].position.x * 2, y: self.size.height/4 - 60)
                } else if numberOfUsers == 2 {
                    labelUser.position = CGPoint(x: self.lines[i+j].position.x * 2, y: self.size.height/4 - 60)
                } else if numberOfUsers == 6 {
                    labelUser.position = CGPoint(x: self.lines[i+j+(2 * j)].position.x * 2, y: self.size.height/4 - 60)
                }
                
                if appDelegate.gamePlay.teams[j].players[i].type == .Machine {
                    labelUser.text = "\((appDelegate.gamePlay.teams[j].players[i] as! MachinePlayer).name)"
                } else {
                    if (appDelegate.gamePlay.teams[j].players[i] as! HumanPlayer).remote.peerID == nil {
                        labelUser.text = (appDelegate.gamePlay.teams[j].players[i] as! HumanPlayer).remote.name
                    } else {
                        labelUser.text = (appDelegate.gamePlay.teams[j].players[i] as! HumanPlayer).remote.peerID?.displayName.gadgetName
                    }
                }
                self.addChild(labelUser)
            }
        }
    }
    
    
    
    func setLabelsScore() {
        self.labelScoreTeam0 = SKLabelNode(text: "0")
        self.labelScoreTeam0.fontColor = .white
        self.labelScoreTeam0.fontSize = 40
        self.labelScoreTeam0.position = CGPoint(x: -50, y: self.size.height/2 - 50)
        self.addChild(labelScoreTeam0)
        
        self.labelScoreTeam1 = SKLabelNode(text: "0")
        self.labelScoreTeam1.fontColor = .white
        self.labelScoreTeam1.fontSize = 40
        self.labelScoreTeam1.position = CGPoint(x: 50, y: self.size.height/2 - 50)
        self.addChild(labelScoreTeam1)
        
        let versus = SKLabelNode(text: "x")
        versus.fontColor = .white
        versus.fontSize = 40
        versus.position = CGPoint(x: 0, y: self.size.height/2 - 50)
        self.addChild(versus)
    }
    
    override func didSimulatePhysics() {
        if endGame == 0 {
            for i in 0..<self.lines.count {
                self.lines[i].move()
            }
        }
    }
    
    func contactArrowMachine(arrow: Arrow) {
        let line = arrow.parent as! ArrowLine
        let numberOfLine = Int(String(line.name!.characters.last!))!
        self.flagsReceive[numberOfLine] = 1
        for i in (line.teamLine?.players)! {
            if i.type == .Machine {
                let playerMachine = i as! MachinePlayer
                if arrow.flagSetScoreMachine == 0 {
                    if appDelegate.gamePlay.teams[0].name ==  line.teamLine!.name {
                        if (playerMachine.randomCorrectGesture(adversaryTeam: appDelegate.gamePlay.teams[1])) {
                            appDelegate.gamePlay.teams[0].score = appDelegate.gamePlay.teams[0].score + 1
                            setTargetTexture(image: #imageLiteral(resourceName: "targetGreen"), indexLine: 0, width: 100)
                            self.animate(team: rightTeam)
                            self.labelScoreTeam0.text = "\(appDelegate.gamePlay.teams[0].victory)"
                        } else {
                            setTargetTexture(image: #imageLiteral(resourceName: "targetRed"), indexLine: 0, width: 100)
                        }
                    } else {
                        if (playerMachine.randomCorrectGesture(adversaryTeam: appDelegate.gamePlay.teams[0])) {
                            appDelegate.gamePlay.teams[1].score = appDelegate.gamePlay.teams[1].score + 1
                            setTargetTexture(image: #imageLiteral(resourceName: "targetGreen"), indexLine: 1, width: 100)
                            self.animate(team: rightTeam)
                            self.labelScoreTeam1.text = "\(appDelegate.gamePlay.teams[1].victory)"
                        } else {
                            setTargetTexture(image: #imageLiteral(resourceName: "targetRed"), indexLine: 1, width: 100)
                        }
                    }
                    arrow.flagSetScoreMachine = 1
                    appDelegate.gamePlay.checkVictory()
                }
            }
        }
    }
    
    func playSong() {
        let path = Bundle.main.path(forResource: "bot", ofType: "mp3")
        song = try? AVAudioPlayer(contentsOf: URL(fileURLWithPath: path!))
        song?.prepareToPlay()
        song?.numberOfLoops = -1 //Makes the song play repeatedly
        song?.play()
    }
    
    func contactArrowCurrentDirection(arrow: Arrow) {
        arrow.setScale(1.1)
        let textureName = "\(arrow.texture)".slice(from: "'", to: "'")
        if "\(arrow.texture)".slice(from: "_", to: "'") == nil {
            arrow.texture = SKTexture(imageNamed: "\(textureName!)_selected")
        }
        let line = arrow.parent as! ArrowLine
        let numberOfLine = Int(String(line.name!.characters.last!))!
        self.currentDirections[numberOfLine] = arrow.directionOfArrow!
    }
    
    func setTargetTexture(image: UIImage, indexLine: Int, width: Int) {
        self.targets[indexLine].texture = SKTexture(image: image)
        self.targets[indexLine].size = CGSize(width: width, height: width)
    }
    func didBegin(_ contact: SKPhysicsContact) {
        let collision: UInt32 = contact.bodyA.categoryBitMask |
            contact.bodyB.categoryBitMask
        if collision == PhysicsCategory.FirstArrow | PhysicsCategory.BottomScreen {
            let lineLoader = LinePattern(screenSize: (self.scene?.size)!)
            var line = ArrowLine()
            if contact.bodyA.categoryBitMask == PhysicsCategory.FirstArrow {
                line = contact.bodyA.node?.parent as! ArrowLine
                contact.bodyA.node?.removeFromParent()
            } else if contact.bodyB.categoryBitMask == PhysicsCategory.FirstArrow {
                line = contact.bodyB.node?.parent as! ArrowLine
                contact.bodyB.node?.removeFromParent()
            }
            lineLoader.loadMoreArrows(line: line)
        } else if collision == PhysicsCategory.Arrow | PhysicsCategory.BottomScreen {
            if contact.bodyA.categoryBitMask == PhysicsCategory.Arrow {
                contact.bodyA.node?.removeFromParent()
            } else if contact.bodyB.categoryBitMask == PhysicsCategory.Arrow {
                contact.bodyB.node?.removeFromParent()
            }
        } else if ((collision == PhysicsCategory.FirstArrow | PhysicsCategory.CheckMachine) || (collision == PhysicsCategory.Arrow | PhysicsCategory.CheckMachine)) {
            if ((contact.bodyA.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyA.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyA.node! as! Arrow
                let line = arrow.parent as! ArrowLine
                if (line.teamLine?.players[0])?.type == .Machine {
                    self.contactArrowMachine(arrow: arrow)
                }
            } else  if ((contact.bodyB.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyB.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyB.node! as! Arrow
                let line = arrow.parent as! ArrowLine
                
                if (line.teamLine?.players[0])?.type == .Machine {
                    self.contactArrowMachine(arrow: arrow)
                }
            }
        } else if ((collision == PhysicsCategory.FirstArrow | PhysicsCategory.CheckReceveid) || (collision == PhysicsCategory.Arrow | PhysicsCategory.CheckReceveid)) {
            if ((contact.bodyA.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyA.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyA.node! as! Arrow
                let line = arrow.parent as! ArrowLine
                let numberOfLine = Int(String(line.name!.characters.last!))!
                if self.flagsReceive[numberOfLine] == 0 {
                    setTargetTexture(image: #imageLiteral(resourceName: "targetRed"), indexLine: numberOfLine, width: 100)
                }
            } else  if ((contact.bodyB.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyB.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyB.node! as! Arrow
                let line = arrow.parent as! ArrowLine
                let numberOfLine = Int(String(line.name!.characters.last!))!
                if self.flagsReceive[numberOfLine] == 0 {
                    setTargetTexture(image: #imageLiteral(resourceName: "targetRed"), indexLine: numberOfLine, width: 100)
                }
            }
        } else if ((collision == PhysicsCategory.Arrow | PhysicsCategory.CurrentDirection) || (collision == PhysicsCategory.FirstArrow | PhysicsCategory.CurrentDirection)) {
            if ((contact.bodyA.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyA.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyA.node! as! Arrow
                let line = arrow.parent as! ArrowLine
                let numberOfLine = Int(String(line.name!.characters.last!))!
                self.flagsReceive[numberOfLine] = 0
                self.contactArrowCurrentDirection(arrow: arrow)
            } else  if ((contact.bodyB.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyB.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyB.node! as! Arrow
                let line = arrow.parent as! ArrowLine
                let numberOfLine = Int(String(line.name!.characters.last!))!
                self.flagsReceive[numberOfLine] = 0
                self.contactArrowCurrentDirection(arrow: arrow)
            }
        } else if ((collision == PhysicsCategory.Arrow | PhysicsCategory.FadeOut) || (collision == PhysicsCategory.FirstArrow | PhysicsCategory.FadeOut)) {
            if ((contact.bodyA.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyA.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyB.node! as! Arrow
                let line = arrow.parent as! ArrowLine
                let numberOfLine = Int(String(line.name!.characters.last!))!
                arrow.setScale(1)
                if "\(arrow.texture)".slice(from: "_", to: "'") == "selected" {
                    arrow.texture = SKTexture(imageNamed: "\("\(arrow.texture)".slice(from: "'", to: "_")!)")
                    self.setTargetTexture(image: #imageLiteral(resourceName: "targetWhite"), indexLine: numberOfLine, width: 80)
                }
                contact.bodyA.node?.run(SKAction.fadeOut(withDuration: 0.5))
            } else  if ((contact.bodyB.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyB.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyB.node! as! Arrow
                let line = arrow.parent as! ArrowLine
                let numberOfLine = Int(String(line.name!.characters.last!))!
                arrow.setScale(1)
                if "\(arrow.texture)".slice(from: "_", to: "'") == "selected" {
                    arrow.texture = SKTexture(imageNamed: "\("\(arrow.texture)".slice(from: "'", to: "_")!)")
                    self.setTargetTexture(image: #imageLiteral(resourceName: "targetWhite"), indexLine: numberOfLine, width: 80)
                }
                contact.bodyB.node?.run(SKAction.fadeOut(withDuration: 0.5))
            }
        }  else if ((collision == PhysicsCategory.Arrow | PhysicsCategory.FadeIn) || (collision == PhysicsCategory.FirstArrow | PhysicsCategory.FadeIn)) {
            if ((contact.bodyA.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyA.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyA.node! as! Arrow
                arrow.setScale(1)
                arrow.flagSetScoreMachine = 0
                if "\(arrow.texture)".slice(from: "_", to: "'") == "selected" {
                    arrow.texture = SKTexture(imageNamed: "\("\(arrow.texture)".slice(from: "'", to: "_")!)")
                }
                contact.bodyA.node?.run(SKAction.fadeIn(withDuration: 1.2))
            } else  if ((contact.bodyB.categoryBitMask == PhysicsCategory.FirstArrow) || (contact.bodyB.categoryBitMask == PhysicsCategory.Arrow)) {
                let arrow = contact.bodyB.node! as! Arrow
                arrow.setScale(1)
                arrow.flagSetScoreMachine = 0
                if "\(arrow.texture)".slice(from: "_", to: "'") == "selected" {
                    arrow.texture = SKTexture(imageNamed: "\("\(arrow.texture)".slice(from: "'", to: "_")!)")
                }
                contact.bodyB.node?.run(SKAction.fadeIn(withDuration: 1.2))
            }
        }
    }
}

extension GameScene: MultipeerDelegate {
    func received(team: Int, remote: RemotePlayer) {
        
    }
    
    func received(controller: Directions, remote: RemotePlayer) {
        if endGame == 0 {
            var indexLine = 0
            for (index,value) in self.lines.enumerated() {
                if value.idUser == remote.id {
                    indexLine = index
                }
            }
            flagsReceive[indexLine] = 1
            print("\(remote.peerID?.displayName): \(controller) -> \(self.currentDirections[indexLine])")
            var indexTeam = 0
            var indexPlayer = 0
            for (iTeam, team) in appDelegate.gamePlay.teams.enumerated() {
                if team.name == self.lines[indexLine].teamLine!.name {
                    indexTeam = iTeam
                    for (iPlayer, player) in appDelegate.gamePlay.teams[iTeam].players.enumerated() {
                        if (player as! HumanPlayer).remote.name == (self.lines[indexLine].player! as! HumanPlayer).remote.name {
                            indexPlayer = iPlayer
                        }
                    }
                }
            }
            DispatchQueue.main.async {
                if controller == self.currentDirections[indexLine] {
                    appDelegate.gamePlay.teams[indexTeam].players[indexPlayer].sumScore()
                    self.setTargetTexture(image: #imageLiteral(resourceName: "targetGreen"), indexLine: indexLine, width: 100)
                    if indexTeam == 0 {
                        self.animate(team: self.leftTeam)
                    } else {
                        self.animate(team: self.rightTeam)
                    }
                    print("Acertou")
                    print("\((appDelegate.gamePlay.teams[indexTeam].players[indexPlayer] as! HumanPlayer).remote.name) = \(controller)")
                    print("\(controller) = \(self.currentDirections[indexLine])")
                    print ("Score: \(appDelegate.gamePlay.teams[indexTeam].score)")
                } else {
                    self.setTargetTexture(image: #imageLiteral(resourceName: "targetRed"), indexLine: indexLine, width: 100)
                }
            }
        }
    }
    
    func peerSentMessage(peer: String, message: String) {
        
    }
    
    func peerConnected() {

    }
    
    func peerDisconnected(remote: RemotePlayer) {
        
    }
}

extension GameScene: GamePlayDelegate {
    func victory(team: Team) {
        endGame = 1
        self.labelVictory.fontColor = .black
        self.labelVictory.fontSize = 40
        self.labelVictory.position = CGPoint(x: 0, y: 0)
        if appDelegate.gamePlay.teams[0].name == team.name {
            self.labelScoreTeam0.text = "\(appDelegate.gamePlay.teams[0].victory)"
        } else {
            self.labelScoreTeam1.text = "\(appDelegate.gamePlay.teams[1].victory)"
        }
        textureTeams.removeAllActions()
        if team.name == "Time 1" {
            self.gameMenu.show(textVictory: "Red Team Won")
        } else {
            self.gameMenu.show(textVictory: "Blue Team Won")
        }
    }
}

extension GameScene: GameMenuDelegate {
    
    func goToMenu() {
        gameMenu.hide()
        song?.stop()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "segue"), object: nil)
    }
    
    func tryAgain() {
        endGame = 0
        for i in 0..<2 {
            appDelegate.gamePlay.teams[i].score = 0
            for j in 0..<appDelegate.gamePlay.teams[i].players.count {
                appDelegate.gamePlay.teams[i].players[j].score = 0
            }
        }
        for line in self.lines {
            line.position = CGPoint(x: line.position.x, y: line.position.y + ( 3/2 * self.size.height/4))
        }
        self.textureTeams.position.x = 0
        for (index, _) in self.targets.enumerated() {
            self.setTargetTexture(image: #imageLiteral(resourceName: "targetWhite"), indexLine: index, width: 80)
        }
        gameMenu.hide()
    }

}
