//
//  HumanPlayer.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 09/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class HumanPlayer: Player {
    var remote: RemotePlayer!
    
    init(name: String, remote: RemotePlayer){
        super.init(name: name, type: .Human)
        self.remote = remote
    }
}
