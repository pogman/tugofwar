//
//  PlayerSprite.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 21/02/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation
import SpriteKit

class BodyNode: SKSpriteNode {
    
    var limbs = [Limb]()
    
    init(texture: SKTexture?) {
        
        super.init(texture: texture, color: UIColor(), size: texture!.size())
        
        self.physicsBody = SKPhysicsBody(rectangleOf: size)
        self.physicsBody?.collisionBitMask = 0x1 << 2
        self.physicsBody?.linearDamping = 1
        self.physicsBody?.allowsRotation = false
        self.physicsBody?.mass = 0.8
        
        self.zPosition = 0
    }
    
    func loadLimbs() {
        
        limbs.forEach {
            
            if let limb = $0 as? SKSpriteNode {
                self.addChild(limb)
            }
        }
    }
    
    override func addChild(_ node: SKNode) {
        super.addChild(node)
        
        if let limb = node as? Limb {
            limb.attach(toBody: self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
