//
//  LinePattern.swift
//  TugOfWarTV
//
//  Created by Luisa Macedo on 2/9/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import SpriteKit

class LinePattern: PatternLoader {
    
    var screenSize: CGSize!
    
    init(screenSize: CGSize){
        self.screenSize = screenSize
    }
    
    func loadMoreArrows(line: ArrowLine) {
        line.createArrows()
    }
    
}
