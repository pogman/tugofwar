//
//  ConnectionsViewController.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 10/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ConnectionsViewController: UIViewController {
    
    @IBOutlet weak var btnReloadRemotes: UIButton!
    
    @IBOutlet weak var btnHelp: UIButton!
    
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var imgTug: UIImageView!
    
    @IBOutlet var lbPlayerNames: [UILabel]!
    
    var teams = [Team(name: "Time 1", color: .blue, players: [Player]()), Team(name: "Time 2", color: .blue, players: [Player]())]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NotificationCenter.default.addObserver(self, selector: #selector(peerDidChangeStateWithNotification), name: NSNotification.Name(rawValue: "MCDidChangeStateNotification"), object: nil)
        
        if let imageView = btnPlay.imageView {
            imageView.adjustsImageWhenAncestorFocused = true
            imageView.clipsToBounds = false
        }
        if let imageView = btnReloadRemotes.imageView {
            imageView.adjustsImageWhenAncestorFocused = true
            imageView.clipsToBounds = false
        }
        
        if let imageView = btnHelp.imageView {
            imageView.adjustsImageWhenAncestorFocused = true
            imageView.clipsToBounds = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelegate.mcManager.delegate = self
        appDelegate.mcManager.session.disconnect()
        appDelegate.mcManager.startBrowsing()
    }
    
    override weak var preferredFocusedView: UIView? {
        return btnPlay
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate.mcManager.browser.stopBrowsingForPeers()
    }
    
    @IBAction func btnHelpAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Tutorial", message: "To play multiplayer you must have the application installed on the iPhone. To do this, download the Robot's War Controller Controller from the AppStore.\n\nOpen TugOfWar Controller and wait for the connection, when your device appears in this screen select your team and water the others connect", preferredStyle: .alert) // 2
        
        let OKAction = UIAlertAction(title: "OK", style: .cancel) { (action) in //3
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true) { // 5
        }
    }
    
    @IBAction func btnReloadRemotesAction(_ sender: Any) {
        appDelegate.mcManager.browser.stopBrowsingForPeers()
        appDelegate.mcManager.delegate = self
        appDelegate.mcManager.session.disconnect()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            appDelegate.mcManager.startBrowsing()
        }
    }
    
    @IBAction func btnPlayAction(_ sender: Any) {
        if ((self.teams[0].players.count == self.teams[1].players.count) && self.teams[0].players.count > 0) {
            appDelegate.gamePlay = GamePlay(teams: self.teams)
            self.performSegue(withIdentifier: "ToPlay", sender: nil)
        }else{
            let alertController = UIAlertController(title: "Multi Player", message: "The number of players is insufficient", preferredStyle: .alert) // 2
            
            let OKAction = UIAlertAction(title: "OK", style: .cancel) { (action) in //3
            }
            
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) { // 5
            }
        }
    }
    
    func verify(team: Int) -> Bool{
        switch team {
        case 0:
            if (teams[0].players.count <= teams[1].players.count) && teams[0].players.count <= 3 {
                return true
            }else{
                return false
            }
        case 1:
            if (teams[1].players.count <= teams[0].players.count) && teams[1].players.count <= 3 {
                return true
            }else{
                return false
            }
        default:
            return false
        }
    }
    
    func loadPlayersName(){
        self.imgTug.image = #imageLiteral(resourceName: "tug1x1")
        for (index, label) in lbPlayerNames.enumerated() {
            label.text = "Searching..."
            
            if index != 0 || index != 3 {
                label.isHidden = true
            }
        }
        
        if teams[0].players.count == 1 || teams[1].players.count == 1 {
            self.imgTug.image = #imageLiteral(resourceName: "tug1x1")
            lbPlayerNames[0].isHidden = false
            lbPlayerNames[3].isHidden = false
        }
        if teams[0].players.count == 2 || teams[1].players.count == 2 {
            self.imgTug.image = #imageLiteral(resourceName: "tug2x2")
            lbPlayerNames[0].isHidden = false
            lbPlayerNames[3].isHidden = false
            lbPlayerNames[1].isHidden = false
            lbPlayerNames[4].isHidden = false
        }
        if teams[0].players.count == 3 || teams[1].players.count == 3 {
            self.imgTug.image = #imageLiteral(resourceName: "tug3x3")
            lbPlayerNames[0].isHidden = false
            lbPlayerNames[3].isHidden = false
            lbPlayerNames[1].isHidden = false
            lbPlayerNames[4].isHidden = false
            lbPlayerNames[2].isHidden = false
            lbPlayerNames[5].isHidden = false
        }
        
        for (index, player) in teams[0].players.enumerated() {
            lbPlayerNames[index].text = (player as! HumanPlayer).remote.peerID?.displayName.gadgetName
            lbPlayerNames[index].isHidden = false
        }
        
        for (index, player) in teams[1].players.enumerated() {
            lbPlayerNames[index+3].text = (player as! HumanPlayer).remote.peerID?.displayName.gadgetName
            lbPlayerNames[index+3].isHidden = false
        }
    }
}

extension ConnectionsViewController: MultipeerDelegate {
    func received(controller: Directions, remote: RemotePlayer) {
        print("\(remote.peerID?.displayName)")
        print(controller)
    }
    
    func received(team: Int, remote: RemotePlayer) {
        if verify(team: team) {
            let player = HumanPlayer(name: "Human", remote: remote)
            
            if let indexPlayer = teams[team].find(player: player) {
                teams[team].players[indexPlayer] = player
            }else{
                teams[team].players.append(player)
            }
            appDelegate.mcManager.sendMessage(acceptTeam: 21, peerID: remote.peerID!)
        }else{
            appDelegate.mcManager.sendMessage(acceptTeam: 20, peerID: remote.peerID!)
        }
        
        DispatchQueue.main.async {
            self.loadPlayersName()
        }
        print(teams[0].players.count)
            
    }
    
    func peerConnected() {
        DispatchQueue.main.async {
            self.loadPlayersName()
        }
    }
    
    func peerDisconnected(remote: RemotePlayer) {
        DispatchQueue.main.async {
            self.teams[0].players.removeAll()
            self.teams[1].players.removeAll()
            self.loadPlayersName()
            print(self.teams[0].players.count)
        }
    }
}
