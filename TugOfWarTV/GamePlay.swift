//
//  GamePlay.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 08/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import UIKit

protocol GamePlayDelegate: class {
    func victory(team: Team)
}

class GamePlay {
    var teams: [Team]!
    
    weak var delegate: GamePlayDelegate!
    
    init(teams: [Team]) {
        self.teams = teams
    }
    
//    func sumScore(team: Team) {
//        if let index = search(team: team) {
//            teams[index].score += 1
//            self.checkVictory()
//        }
//    }
    
    private func sumScore() {
        for (index, team) in teams.enumerated() {
            if team.players[0].type! != .Machine {
                teams[index].score = 0
                for player in team.players{
                    teams[index].score += player.score
                }
            }
        }
    }
    
    func checkVictory() {
        let valueForVictory = 10
        if let teams = self.teams {
            self.sumScore()
            if (teams[0].score - teams[1].score) > valueForVictory {
                self.teams[1].score = 0
                self.teams[0].score = 0
                self.teams[0].victory += 1
                
                self.delegate.victory(team: teams[0])
            }else if (teams[1].score - teams[0].score) > valueForVictory {
                self.teams[0].score = 0
                self.teams[1].score = 0
                self.teams[1].victory += 1
                
                self.delegate.victory(team: teams[1])
            }
        }
    }
    
    private func search(team: Team) -> Int? {
        for (index, searchTeam) in teams.enumerated() {
            if searchTeam.name == team.name {
                return index
            }
        }
        return nil
    }
    
}




