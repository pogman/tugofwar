//
//  FocusControl.swift
//  Control
//
//  Created by Pedro Albuquerque on 02/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import UIKit

class FocusControl: UIControl {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = true
        self.layer.cornerRadius = 20
        self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.backgroundColor = self.backgroundColor?.withAlphaComponent(0.5)
    }
    
    lazy private var motionEffectGroup: UIMotionEffectGroup = {
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        
        horizontalMotionEffect.minimumRelativeValue = -8.0
        horizontalMotionEffect.maximumRelativeValue = 8.0
        
        
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        
        verticalMotionEffect.minimumRelativeValue = -8.0
        verticalMotionEffect.maximumRelativeValue = 8.0
        
        let motionEffectGroup = UIMotionEffectGroup()
        motionEffectGroup.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        
        return motionEffectGroup
    }()
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedItem === self {
            coordinator.addCoordinatedAnimations({
                
                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                self.layer.shadowOpacity = 0.2
                self.layer.shadowOffset = CGSize(width: 0, height: 15)
                self.backgroundColor = self.backgroundColor?.withAlphaComponent(1.0)
                self.addMotionEffect(self.motionEffectGroup)
                
            }, completion: nil)
            
        }else if context.previouslyFocusedItem === self {
            coordinator.addCoordinatedAnimations({
                
                self.transform = CGAffineTransform.identity
                self.layer.shadowOpacity = 0
                self.backgroundColor = self.backgroundColor?.withAlphaComponent(0.5)
                self.removeMotionEffect(self.motionEffectGroup)
                
            }, completion: nil)
        }
    }
}
