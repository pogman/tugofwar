//
//  Team.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 02/03/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation
import SpriteKit

/*enum TeamColor {
    case blue
    case red
}*/

class TeamNode {
    
    var players = [PlayerNode]()
    var numberOfPlayers: Int
    var color: Colors!
    
    init(of numberOfPlayers: Int, color: Colors, inside scene: SKScene) {
        self.color = color
        self.numberOfPlayers = numberOfPlayers
        
        (1...numberOfPlayers).forEach { _ in
            players.append(PlayerNode(ofTeam: color))
        }
        
        players.forEach { player in
            scene.addChild(player)
            player.loadLimbs()
        }
    }
    
    func pull() {
        players.forEach { $0.pull() }
    }
    
    func bePulled() {
        players.forEach { $0.bePulled() }
    }
}
