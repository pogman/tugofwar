//
//  PlayerSelectCell.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 13/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import UIKit

class PlayerSelectCell: UICollectionViewCell {

    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
    }
    
    func setImage(pos: Int) {
        if pos % 2 == 0 {
            image.image = #imageLiteral(resourceName: "blue")
        }else{
            image.image = #imageLiteral(resourceName: "green")
        }
    }

}
