//
//  BodyNode+Animation.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 22/02/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation
import SpriteKit

extension PlayerNode {
    
    func pull() {
        
        let vectorOrientation = (self.team == .blue) ? pullingForce : pullingForce * -1
        let angleOrientation = (self.team == .blue) ? 15 : -15
        
        self.run(SKAction.rotate(byAngle: -angleOrientation.radianValue, duration: TimeInterval(0.3))) {
            self.run(SKAction.rotate(byAngle: angleOrientation.radianValue, duration: TimeInterval(0.3)))
        }
        
        self.run(SKAction.applyImpulse(CGVector(dx: vectorOrientation, dy: 0), duration: TimeInterval(0.3)))
    }
    
    func bePulled() {
        
        let angleOrientation = (self.team == .blue) ? 25 : -25
        
        self.run(SKAction.rotate(byAngle: angleOrientation.radianValue, duration: TimeInterval(0.3))) {
            self.run(SKAction.rotate(byAngle: -angleOrientation.radianValue, duration: TimeInterval(0.3)))
        }
    }
}
