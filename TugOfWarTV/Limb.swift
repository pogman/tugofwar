//
//  Limb.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 22/02/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation

protocol Limb: class {
    func attach(toBody body: BodyNode)
}
