//
//  Player.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 09/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity

enum PlayerType {
    case Human, Machine
}

class Player {
    let name: String
    let type: PlayerType!
    var score = 0
    
    init(name: String, type: PlayerType) {
        self.name = name
        self.type = type
    }
    
    func sumScore() {
        self.score += 1
        appDelegate.gamePlay.checkVictory()
    }
}

class RemotePlayer {
    let id: String
    let name: String
    let peerID: MCPeerID?
    
    init(id: String, name: String, peerID: MCPeerID?) {
        self.id = id
        self.name = name
        self.peerID = peerID
    }
}

struct Team {
    var name: String
    var color: UIColor
    var score: Int
    var victory: Int
    
    var players: [Player]
    
    init(name: String, color: UIColor, players: [Player]) {
        self.name = name
        self.color = color
        self.score = 0
        self.victory = 0
        self.players = players
    }
    
    func find(player: HumanPlayer) -> Int? {
        for (index, p) in (players as! [HumanPlayer]).enumerated() {
            if p.remote.id == player.remote.id {
                return index
            }
        }
        return nil
    }
}
