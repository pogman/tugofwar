//
//  HeadNode.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 21/02/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation
import SpriteKit

class HeadNode: SKSpriteNode, Limb {
    
    init(texture: SKTexture?) {
        super.init(texture: texture, color: UIColor(), size: texture!.size())
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0)
        
        self.physicsBody = SKPhysicsBody(rectangleOf: size, center: self.centerPoint)
        self.physicsBody?.collisionBitMask = 0x1 << 3
        self.physicsBody?.categoryBitMask = 0x1 << 3
        self.physicsBody?.mass = 0.001
        
        self.zPosition = 3
    }
    
    func attach(toBody body: BodyNode) {
        
        let anchorPoint = body.convert(CGPoint(x: body.frame.midX, y: body.frame.maxY / 1.3), to: body.scene!)
        
        self.position = anchorPoint
        
        let joint = SKPhysicsJointPin.joint(withBodyA: self.physicsBody!, bodyB: body.physicsBody!, anchor: anchorPoint)
        
        joint.shouldEnableLimits = true
        joint.upperAngleLimit = 10.radianValue
        joint.lowerAngleLimit = -10.radianValue
        
        body.scene!.physicsWorld.add(joint)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
