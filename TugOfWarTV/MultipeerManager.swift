//
//  MultipeerManager
//  TugOfWar
//
//  Created by Pedro Albuquerque on 14/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserve.
//

import UIKit
import MultipeerConnectivity

protocol MultipeerDelegate {
    func peerConnected()
    func peerDisconnected(remote: RemotePlayer)
    func peerSentMessage(peer: String, message: String)
    func received(controller: Directions, remote: RemotePlayer)
    func received(team: Int, remote: RemotePlayer)
}

extension MultipeerDelegate {
    func peerSentMessage(peer: String, message: String) { }
}

class MultipeerManager: NSObject {
    
    var delegate: MultipeerDelegate!
    
    var arrConnectedDevices = [RemotePlayer]()
    var arrFoundDevices = [RemotePlayer]()
    
    var peer = MCPeerID.init(displayName: UIDevice.current.name)
    
    let serviceTypePadrao = "tug"
    
    var browser: MCNearbyServiceBrowser
    
    override init() {
        
        browser = MCNearbyServiceBrowser.init(peer: peer, serviceType: serviceTypePadrao)
        
        super.init()
        
        browser.delegate = self
    }
    
    lazy var session: MCSession = {
        let sessao = MCSession.init(peer: self.peer)
        sessao.delegate = self
        return sessao
    }()
    
    func startBrowsing(){
        browser.startBrowsingForPeers()
    }
    
    func sendMessage(acceptTeam: Int, peerID: MCPeerID){
        let dataToSend: Data? = Data(from: acceptTeam)
        //let allPeers: [Any] = appDelegate.mcManager.session.connectedPeers
        do{
            try appDelegate.mcManager.session.send(dataToSend!, toPeers: [peerID], with: .reliable)
        }catch{
            print("Nao foi")
        }
    }
    
    func find(peerID: MCPeerID, remotes: [RemotePlayer]) -> Int? {
        for (index, remote) in remotes.enumerated() {
            if remote.peerID == peerID {
                return index
            }
        }
        return nil
    }
}

extension MultipeerManager: MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        
        if let key = info?["key"] {
            let newRemotePlayer = RemotePlayer(id: key, name: peerID.displayName, peerID: peerID)
            if let remoteIndex = find(peerID: newRemotePlayer.peerID!, remotes: arrFoundDevices){
                arrFoundDevices[remoteIndex] = newRemotePlayer
            }else{
                arrFoundDevices.append(newRemotePlayer)
                print("cima\(arrConnectedDevices)")
                if find(peerID: peerID, remotes: arrConnectedDevices) == nil {
                    self.browser.invitePeer(peerID, to: session, withContext: nil, timeout: 60)
                }
            }
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {}
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) { }
}


extension MultipeerManager: MCSessionDelegate {
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        let dataValue = data.to(type: Int.self)
        if dataValue < 10 {
            let receivedController = Directions(rawValue: (dataValue))
            if let remoteIndex = find(peerID: peerID, remotes: arrConnectedDevices) {
                delegate.received(controller: receivedController!, remote: arrConnectedDevices[remoteIndex])
            }
        }else if dataValue < 20 {
            if let remoteIndex = find(peerID: peerID, remotes: arrConnectedDevices) {
                delegate.received(team: dataValue-10, remote: arrConnectedDevices[remoteIndex])
            }
        }
    }
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        
        switch state {
        case .connected:
            
            // PEER CONNECTED
            if let remoteIndex = find(peerID: peerID, remotes: arrFoundDevices) {
                self.arrConnectedDevices.append(arrFoundDevices[remoteIndex])
                delegate.peerConnected()
            }
        case .connecting:
            // PEER IS CONNECTING
            break
        case .notConnected:
            
            // PEER DISCONNECTED
            
            if let remoteIndex = find(peerID: peerID, remotes: arrConnectedDevices) {
                self.arrConnectedDevices.remove(at: remoteIndex)
            }
            
            if let remoteIndex = find(peerID: peerID, remotes: arrFoundDevices) {
                delegate?.peerDisconnected(remote: arrFoundDevices[remoteIndex])
                arrFoundDevices.remove(at: remoteIndex)
            }
        }
        print("baixo\(arrConnectedDevices)")
        
    }
    
    func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void) {
        certificateHandler(true)
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) { }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL, withError error: Error?) { }
    
}
