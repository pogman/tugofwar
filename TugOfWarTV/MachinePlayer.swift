//
//  MachinePlayer.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 09/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation

enum MachinePlayerLevel: Int {
    case automatic = 0, easy = 1, medium = 2, hard = 4
}

class MachinePlayer: Player {
    var machineLevel: MachinePlayerLevel!
    var machineLevelAutomatic = false
    
    init(level: MachinePlayerLevel) {
        super.init(name: "Machine", type: .Machine)
        self.machineLevel = level
        if self.machineLevel == MachinePlayerLevel.automatic {
            self.machineLevel = MachinePlayerLevel.easy
            self.machineLevelAutomatic = true
        }
    }
    
    func randomCorrectGesture(adversaryTeam: Team) -> Bool {
        let random = Int(arc4random_uniform(6) + 0)
        if machineLevelAutomatic {
            switchLevel(adversaryTeam: adversaryTeam)
        }
        if random <= self.machineLevel.rawValue {
            return true
        }else {
            return false
        }
    }
    
    private func switchLevel(adversaryTeam: Team) {
        let scoreForPlayer = (adversaryTeam.score/adversaryTeam.players.count)
        if scoreForPlayer > 25 {
            self.machineLevel = MachinePlayerLevel.medium
        }else if scoreForPlayer > 50 {
            self.machineLevel = MachinePlayerLevel.hard
        }
    }
}
