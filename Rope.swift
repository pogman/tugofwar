//
//  DividedRope.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 15/03/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation
import SpriteKit

enum RopeError: Error {
    case invalidCombinationOfTeams(String)
}

enum RopeSize: Int {
    case small = 45
    case large = 80
    case veryLarge = 100
}

class Rope: SKSpriteNode {
    
    var pieces = [RopePiece]()
    
    var ropeSize: RopeSize!
    var positionOffset: CGFloat!
    
    init(ofSize ropeSize: RopeSize) {
        super.init(texture: SKTexture(), color: UIColor(), size: SKTexture().size())
        
        self.ropeSize = ropeSize
        
        switch(ropeSize) {
            
        case .small:
            positionOffset = 0.2
            
        case .large:
            positionOffset = 0.41
            
        case .veryLarge:
            positionOffset = 0.5
            
        }
        
        (0..<ropeSize.rawValue).forEach { i in
            let piece = RopePiece()
            
            self.addChild(piece)
            pieces.append(piece)
        }
    }
    
    func draw() {
        
        pieces.first!.position = CGPoint(x: scene!.frame.minX * positionOffset, y: 0)
        
        for (i, piece) in pieces.enumerated() {
            
            if pieces.index(after: i) < pieces.endIndex {
                let nextPiece = pieces[pieces.index(after: i)]
                
                let anchorPoint = convert(piece.rightAnchorPoint, to: scene!)
                nextPiece.position = CGPoint(x: anchorPoint.x + piece.frame.width / 4, y: anchorPoint.y)
                
                let joint = SKPhysicsJointPin.joint(withBodyA: nextPiece.physicsBody!, bodyB: piece.physicsBody!, anchor: anchorPoint)
                
                joint.shouldEnableLimits = true
                joint.lowerAngleLimit = 0.radianValue
                joint.upperAngleLimit = 5.radianValue
                
                self.scene?.physicsWorld.add(joint)
            }
        }
    }
    
    func attach(leftTeam: TeamNode, rightTeam: TeamNode) throws {
        
        if leftTeam.numberOfPlayers != rightTeam.numberOfPlayers {
            throw RopeError.invalidCombinationOfTeams("The teams you're trying to attach have different number of players.")
        }
        
        let playerOffset = 20
        
        let leftTeamJoints: [SKPhysicsJoint] = leftTeam.players.enumerated().map { i, player in
            
            let piece = pieces[(i == 0) ? 0 : i * playerOffset]
            player.position = convert(piece.leftAnchorPoint, to: scene!)
            
            return SKPhysicsJointPin.joint(withBodyA: player.arm.physicsBody!, bodyB: piece.physicsBody!, anchor: player.position)
        }
        
        let rightTeamJoints: [SKPhysicsJoint] = rightTeam.players.enumerated().map { i, player in
            
            let lastIndex = pieces.index(of: pieces.last!)!
            
            let piece = pieces[(i == 0) ? lastIndex : lastIndex - (i * playerOffset)]
            player.position = convert(piece.rightAnchorPoint, to: scene!)
            
            return SKPhysicsJointPin.joint(withBodyA: player.arm.physicsBody!, bodyB: piece.physicsBody!, anchor: player.position)
        }
        
        (leftTeamJoints + rightTeamJoints).forEach { joint in self.scene!.physicsWorld.add(joint) }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
