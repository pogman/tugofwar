//
//  RopePiece.swift
//  TugOfWar-sandbox
//
//  Created by Murilo da Paixão on 15/03/17.
//  Copyright © 2017 Murilo da Paixão. All rights reserved.
//

import Foundation
import SpriteKit

class RopePiece: SKSpriteNode {
    
    var leftAnchorPoint: CGPoint {
        return CGPoint(x: frame.minX, y: frame.midY)
    }
    
    var rightAnchorPoint: CGPoint {
        return CGPoint(x: frame.maxX, y: frame.midY)
    }
    
    init() {
        
        let texture = SKTexture(image: #imageLiteral(resourceName: "rope_piece"))
        super.init(texture: texture, color: UIColor(), size: texture.size())
        
        self.physicsBody = SKPhysicsBody(rectangleOf: size, center: self.centerPoint)
        self.physicsBody?.mass = 0.005
        self.physicsBody?.categoryBitMask = 0
        self.physicsBody?.collisionBitMask = 0
        
        self.zPosition = 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
