//
//  RopeNode.swift
//  TugOfWarTV
//
//  Created by Murilo da Paixão on 09/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import SpriteKit

class RopeNode: SKSpriteNode {
    
    init(_ texture: SKTexture) {
        super.init(texture: texture, color: UIColor(), size: texture.size())
        
        self.anchorPoint = CGPoint(x: 0.5, y: 1)
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        
        self.physicsBody!.isDynamic = true
        self.physicsBody!.affectedByGravity = false
        self.physicsBody!.allowsRotation = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
